import './App.css';
import React, { Component } from 'react';
import Authenticate from 'react-openidconnect';
import OidcSettings from './oidcSettings';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { useEffect } from 'react';
import { useState } from 'react';

const Authenticated = props => {
  if (props !== undefined && props.user !== undefined) {
    return (
      <div>
        Olá {props.user.profile.name.split(' ')[0]}
        <Saldo token={props.user.access_token}></Saldo>
      </div>
    );
  }
  return null;
};

const Saldo = (props) => {
  const [saldo, setsaldo] = useState(0);

  useEffect(() => {
    //fetch('https://localhost:5002/Statement', {
      fetch('https://bancopanapi.azurewebsites.net/Statement', {
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ' + props.token
      },
    }).then(response => {
      if(response.ok){
        return response.json();
      }
    }).then(data => {
      setsaldo(data.saldo);
    });
  }, []);


  return (<div>Saldo atual {saldo}</div>);
}

class App extends Component {

  constructor(props) {
    super(props);
    this.userLoaded = this.userLoaded.bind(this);
    this.userUnLoaded = this.userUnLoaded.bind(this);

    this.state = { user: undefined };
  }

  userLoaded(user) {
    if (user)
      this.setState({ "user": user });
  }

  userUnLoaded() {
    this.setState({ "user": undefined });
  }

  NotAuthenticated() {
    return (
      <div>
        <Button variant="primary" size="lg" className="btn">
          Login
        </Button>
      </div>
    );
  }

  render() {
    return (

      <Container>
        <Row className="content">
          <Col sm={10}>
            <img src="/assets/imgs/logo-new.svg" className="logo" alt='logo' />
          </Col>
          <Col sm={2}>
            <Authenticate OidcSettings={OidcSettings} userLoaded={this.userLoaded} userunLoaded={this.userUnLoaded} renderNotAuthenticated={this.NotAuthenticated}>
              <Authenticated user={this.state.user} />
            </Authenticate>
          </Col>
        </Row>
        <Row className="content">
          <Col>
            <img src="/assets/imgs/banner_1.jpg" className="banner" alt='banner' />
          </Col>
        </Row>
      </Container>

    )
  }
}

export default App;
