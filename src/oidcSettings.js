var OidcSettings = {    
    authority: "https://bancopanlogin.azurewebsites.net",
    client_id: "conta_digital",
    redirect_uri: "https://calm-sand-05e305710.1.azurestaticapps.net",
    post_logout_redirect_uri: "https://calm-sand-05e305710.1.azurestaticapps.net",
    response_type: 'id_token token',
    scope: "openid profile extrato"   
};

// var OidcSettings = {    
//     authority: "https://localhost:5001",
//     client_id: "conta_digital_local",
//     redirect_uri: "http://localhost:3000",
//     post_logout_redirect_uri: "http://localhost:3000",
//     response_type: 'id_token token',
//     scope: "openid profile extrato"   
// };

export default OidcSettings;