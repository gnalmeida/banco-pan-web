# Introdução 
O Banco Pan Web é o serviço de Conta Digital que se autentica no Banco Pan Login(SSO), feito para o chalenge final do MBA em engenharia de Software da FIAP.

# Tecnologias utilizadas:
- React Js
- react-openidconnect

## Comandos para executar o projeto:
- npm install
- npm start
